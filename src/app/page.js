"use client";

import Image from "next/image";
import { Inter } from "next/font/google";
import React, { useState } from "react";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  const [modalOneActive, setModalOneActive] = useState(false);
  const [modalTwoActive, setModalTwoActive] = useState(false);

  return (
    <>
      <section className="absolute inset-0 grid place-content-center">
        <div className="flex gap-2 items-center">
          <button
            type="button"
            className="button button-primary"
            onClick={() => setModalOneActive(true)}
          >
            <span>Open CSS Rank Up</span>
          </button>
          <button
            type="button"
            className="button button-primary"
            onClick={() => setModalTwoActive(true)}
          >
            <span>Open Video Rank Up</span>
          </button>
        </div>
      </section>

      {modalOneActive && (
        <main className={`modal`}>
          <section className="modal-content surface">
            <button
              type="button"
              className="modal-close button button-close"
              onClick={() => setModalOneActive(false)}
            >
              <span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="48"
                  height="48"
                  viewBox="0 0 24 24"
                >
                  <path d="M16.707,7.293c-.391-.391-1.023-.391-1.414,0l-3.293,3.293-3.293-3.293c-.391-.391-1.023-.391-1.414,0s-.391,1.023,0,1.414l3.293,3.293-3.293,3.293c-.391,.391-.391,1.023,0,1.414,.195,.195,.451,.293,.707,.293s.512-.098,.707-.293l3.293-3.293,3.293,3.293c.195,.195,.451,.293,.707,.293s.512-.098,.707-.293c.391-.391,.391-1.023,0-1.414l-3.293-3.293,3.293-3.293c.391-.391,.391-1.023,0-1.414Z"></path>
                </svg>
              </span>
            </button>
            <div className="rankup-container">
              <h3 className="rankup-congratulations">
                <span className="blockreveal">
                  <span>Congratulations</span>
                </span>
                <span className="blockreveal">
                  <span>you have been promoted</span>
                </span>
              </h3>

              <div className={`rankup-illustration w-[350px]`}>
                <video autoPlay playsinline muted preload>
                  <source
                    src="/assets/videos/Particles.webm"
                    type="video/webm"
                  />
                </video>
                <div className="rankup">
                  <i
                    style={{
                      "-webkit-mask-image": "url(/assets/images/Rank_mask.png)",
                    }}
                  />
                  <i
                    style={{
                      "-webkit-mask-image":
                        "url(/assets/images/Rank_Icons_4.png)",
                    }}
                  />
                  <i
                    style={{
                      "-webkit-mask-image":
                        "url(/assets/images/Rank_Icons_5.png)",
                    }}
                  />

                  <img
                    src="/assets/images/Rank_Icons_0.png"
                    alt=""
                    className="-z-50 rankup-bottom-3"
                  />
                  <img
                    src="/assets/images/Rank_Icons_1.png"
                    alt=""
                    className="-z-40 rankup-bottom-2"
                  />
                  <img
                    src="/assets/images/Rank_Icons_2.png"
                    alt=""
                    className="-z-30 rankup-bottom-1"
                  />
                  <img
                    src="/assets/images/Rank_Icons_3.png"
                    alt=""
                    className="-z-20 rankup-top"
                  />
                  <img
                    src="/assets/images/Rank_Icons_wing_l.png"
                    alt=""
                    className="-z-10 rankup-wing-l"
                  />
                  <img
                    src="/assets/images/Rank_Icons_wing_r.png"
                    alt=""
                    className="-z-10 rankup-wing-r"
                  />
                  <img
                    src="/assets/images/Rank_Icons_4.png"
                    alt=""
                    className="z-10 rankup-shield"
                  />
                  <img
                    src="/assets/images/Rank_Icons_5.png"
                    alt=""
                    className="z-20 rankup-level"
                  />
                </div>
              </div>

              <h4 className="rankup-rank">
                <span className="blockreveal">
                  <span>Immortal</span>
                </span>
              </h4>
            </div>
          </section>
          <button
            type="button"
            className="modal-backdrop"
            onClick={() => setModalOneActive(false)}
          >
            <video autoPlay playsinline muted preload>
              <source src="/assets/videos/Particles.webm" type="video/webm" />
            </video>
          </button>
        </main>
      )}
      {modalTwoActive && (
        <main className={`modal ${modalTwoActive ? "is-active" : ""}`}>
          <section className="modal-content surface">
            <button
              type="button"
              className="modal-close button button-close"
              onClick={() => setModalTwoActive(false)}
            >
              <span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="48"
                  height="48"
                  viewBox="0 0 24 24"
                >
                  <path d="M16.707,7.293c-.391-.391-1.023-.391-1.414,0l-3.293,3.293-3.293-3.293c-.391-.391-1.023-.391-1.414,0s-.391,1.023,0,1.414l3.293,3.293-3.293,3.293c-.391,.391-.391,1.023,0,1.414,.195,.195,.451,.293,.707,.293s.512-.098,.707-.293l3.293-3.293,3.293,3.293c.195,.195,.451,.293,.707,.293s.512-.098,.707-.293c.391-.391,.391-1.023,0-1.414l-3.293-3.293,3.293-3.293c.391-.391,.391-1.023,0-1.414Z"></path>
                </svg>
              </span>
            </button>
            <div className="rankup-container">
              <h3 className="rankup-congratulations">
                <span className="blockreveal">
                  <span>Congratulations</span>
                </span>
                <span className="blockreveal">
                  <span>you have been promoted</span>
                </span>
              </h3>

              <div className="rankup-video">
                <video autoPlay playsinline muted preload>
                  <source
                    src="/assets/videos/Particles.webm"
                    type="video/webm"
                  />
                </video>
                <video autoPlay playsinline muted preload>
                  <source
                    src="/assets/videos/Rank_Silver_3_noparticles.webm"
                    type="video/webm"
                  />
                </video>
              </div>

              <h4 className="rankup-rank">
                <span className="blockreveal">
                  <span>Immortal</span>
                </span>
              </h4>
            </div>
          </section>
          <button
            type="button"
            className="modal-backdrop"
            onClick={() => setModalTwoActive(false)}
          >
            <video autoPlay playsinline muted preload>
              <source src="/assets/videos/Particles.webm" type="video/webm" />
            </video>
          </button>
        </main>
      )}
      {/*
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className="z-10 w-full max-w-5xl items-center justify-between font-mono text-sm lg:flex">
        <p className="fixed left-0 top-0 flex w-full justify-center border-b border-gray-300 bg-gradient-to-b from-zinc-200 pb-6 pt-8 backdrop-blur-2xl dark:border-neutral-800 dark:bg-zinc-800/30 dark:from-inherit lg:static lg:w-auto  lg:rounded-xl lg:border lg:bg-gray-200 lg:p-4 lg:dark:bg-zinc-800/30">
          Hello Get started by editing&nbsp;
          <code className="font-mono font-bold">src/app/page.tsx</code>
        </p>
        <div className="fixed bottom-0 left-0 flex h-48 w-full items-end justify-center bg-gradient-to-t from-white via-white dark:from-black dark:via-black lg:static lg:h-auto lg:w-auto lg:bg-none">
          <a
            className="pointer-events-none flex place-items-center gap-2 p-8 lg:pointer-events-auto lg:p-0"
            href="https://vercel.com?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
            target="_blank"
            rel="noopener noreferrer"
          >
            By{' '}
            <Image
              src="/vercel.svg"
              alt="Vercel Logo"
              className="dark:invert"
              width={100}
              height={24}
              priority
            />
          </a>
        </div>
      </div>

      <div className="relative flex place-items-center before:absolute before:h-[300px] before:w-[480px] before:-translate-x-1/2 before:rounded-full before:bg-gradient-radial before:from-white before:to-transparent before:blur-2xl before:content-[''] after:absolute after:-z-20 after:h-[180px] after:w-[240px] after:translate-x-1/3 after:bg-gradient-conic after:from-sky-200 after:via-blue-200 after:blur-2xl after:content-[''] before:dark:bg-gradient-to-br before:dark:from-transparent before:dark:to-blue-700 before:dark:opacity-10 after:dark:from-sky-900 after:dark:via-[#0141ff] after:dark:opacity-40 before:lg:h-[360px]">
        <Image
          className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
          src="/next.svg"
          alt="Next.js Logo"
          width={180}
          height={37}
          priority
        />
      </div>

      <div className="mb-32 grid text-center lg:mb-0 lg:grid-cols-4 lg:text-left">
        <a
          href="https://beta.nextjs.org/docs?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Docs{' '}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Find in-depth information about Next.js features and API.
          </p>
        </a>

        <a
          href="https://nextjs.org/learn?utm_source=create-next-app&utm_medium=appdir-template-tw&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800 hover:dark:bg-opacity-30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Learn{' '}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Learn about Next.js in an interactive course with&nbsp;quizzes!
          </p>
        </a>

        <a
          href="https://vercel.com/templates?framework=next.js&utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Templates{' '}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Explore the Next.js 13 playground.
          </p>
        </a>

        <a
          href="https://vercel.com/new?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Deploy{' '}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Instantly deploy your Next.js site to a shareable URL with Vercel.
          </p>
        </a>
      </div>
    </main>
    */}
    </>
  );
}
