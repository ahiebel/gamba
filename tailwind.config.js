/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        "primary-gradient":
          "radial-gradient(79.44% 190.83% at 47.12% 105.83%, #4affd4 0%, #008976 100%)",
        "primary-gradient-2":
          "radial-gradient(circle at 47% 106%, #4affd4,#008976 21%)",
        "primary-hover-gradient":
          "radial-gradient(79.44% 190.83% at 47.12% 105.83%,#97ffe6 0%, #0abca3 100%)",
        "third-gradient":
          "radial-gradient(64.23% 101.67% at 47.73% 94.17%,#ffdb41 0%, #f0b91e)",
        "third-hover-gradient":
          "radial-gradient(64.23% 101.67% at 47.73% 94.17%, #fae175 0%, #ffd04b 100%)",
        "secondary-gradient":
          "linear-gradient(180deg, #343a47 0%, #2c3140 100%)",
        "secondary-hover-gradient":
          "linear-gradient(180deg, #434b5b 0%, #33394a 100%)",
        "danger-gradient":
          "radial-gradient(776.29% 137.75% at -64.15% 71.86%,#f96868 0%,#f80000 100% )",
        "danger-hover-gradient":
          "radial-gradient(776.29% 137.75% at -64.15% 71.86%, #ff8484 0%, #f01919 100% )",
        "rect-border-gradient":
          "linear-gradient(180deg,#2a2e39 0%,rgba(33, 37, 48, 1) 15%,rgba(33, 37, 48, 1) 100%);",
      },
      boxShadow: {
        "primary-box": "0 6.2px 18.5px 0 rgba(54, 190, 6, 0.11)",
        "third-box": "0 6.2px 18.5px 0 rgba(242, 190, 35, 0.11)",
        "secondary-box": "0 7.5px 31.1px 0 rgba(0, 0, 0, 0)",
        "dark-box": "inset 0 4px 4px 0 rgba(0, 0, 0, 0.08)",
        "steel-box": "inset 0 4px 6px rgba(0, 0, 0, 0.25)",
        "progress-box": "0px 6.1792px 18.5376px rgba(54, 190, 6, 0.11)",
        "radio-box": "0px 5.08929px 15.2679px rgba(54, 190, 6, 0.11)",
        "popover": "0px 4px 4px rgba(0, 0, 0, 0.07)"
      },
      transitionTimingFunction: {
        "in-expo": "cubic-bezier(0.4, 0, 0.23, 1)",
      },
      fontSize: {
        xxs: ["0.625rem", "0.75rem"], // 10px 12px
        xs: ["0.75rem", "0.9375rem"], // 12px, 15px
        sm: ["0.875rem", "1.0625rem"], // 14px, 17px
        base: ["1rem", "1.1875rem"], // 16px, 19px
      },
      borderRadius: {
        lg: "0.625rem", // 10px
        md: "0.4375rem", // 7px
      },
      screens: {
        sm: "375px",
        lg: "1440px",
      },
    },
    colors: {
      transparent: "transparent",
      "primary-color": "#67fde2",
      "primary-hover-color": "#91ffeb",
      "third-color": "#ffdb95",
      "third-hover-color": "#ffe1a8",
      "secondary-color": "#3f4655",
      "secondary-hover-color": "#485061",
      "danger-color": "rgba(255, 255, 255, 0.44)",
      "danger-hover-color": "rgba(255, 255, 255, 0.32)",
      "dark-color": "#1c202c",
      "black-russian-color": "#171b23",
      "slate-color": "#7c8497",
      "steel-color": "#858da0",
      "ronil-color": "#00af96",
      "dark-grey-color": "rgba(33, 37, 48, 0.6)",
      "midnight-express-color": "#272a33",
      "ebony-clay-color": "#212530",
      "ebony-color": "#2a2e39",
      "concrete-gray-grey-color": "#f3f3f3",
      "cloud-burst-color": "rgba(55, 60, 71, 0.53)",
      white: "#fff",
      black: "#000"
    },
    fontFamily: {
      proximanova: ["proximanova", "sans-serif"],
      sans: ["proximanova", "sans-serif"], // To apply default font across global
    },
    animation: {
      "slide-in-left": "slideInLeft 0.88s cubic-bezier(0.7, 0, 0.1, 1) both",
      "slide-in-right":
        "slideInRight 0.88s cubic-bezier(0.7, 0, 0.1, 1) both",
      "slide-in-top": "slideInTop 0.88s cubic-bezier(0.7, 0, 0.1, 1) both",
      "slide-in-bottom":
        "slideInBottom 0.88s cubic-bezier(0.7, 0, 0.1, 1) both",
      "fade-in": "fadeIn 0.88s cubic-bezier(0.7, 0, 0.1, 1) both",
      "fade-out": "fadeOut 0.44s cubic-bezier(0.7, 0, 0.1, 1) both",
      "scale-in": "scaleIn 0.88s cubic-bezier(0.7, 0, 0.1, 1) both",
      "scale-out": "scaleOut 0.44s cubic-bezier(0.7, 0, 0.1, 1) both",
      heartbeat: "heartbeat 1.5s ease-in-out infinite both",
      "drop-in": "dropIn 1.1s both",
      "bounce-right": "bounceRight 1.1s infinite",
      "bounce-left": "bounceLeft 1.1s infinite",
      "bounce-bottom": "bounceBottom 1.1s infinite",
      rotate: "rotate 12s linear infinite",
      floating: "floating 6s cubic-bezier(.45,0,.4,1) infinite",
      shake: "shake 0.8s cubic-bezier(.455,.030,.515,.955) both",
      "scale-in-x-left": "scaleInXLeft 0.75s cubic-bezier(0.7, 0, 0.1, 1) both",
      "scale-out-x-left": "scaleOutXLeft 0.75s cubic-bezier(0.7, 0, 0.1, 1) both",
      "scale-in-x-right": "scaleInXRight 0.75s cubic-bezier(0.7, 0, 0.1, 1) both",
      "scale-out-x-right": "scaleOutXRight 0.75s cubic-bezier(0.7, 0, 0.1, 1) both",
      "fire-in": "fireIn 0.88s cubic-bezier(0.7, 0, 0.1, 1) both",
      "fire-out": "fireOut 0.44s cubic-bezier(0.7, 0, 0.1, 1) both",
    },
    keyframes: {
      slideInLeft: {
        "0%": {
          transform: "translateX(-2rem)",
          opacity: "0",
        },
        "100%": {
          transform: "translateX(0)",
          opacity: "1",
        },
      },
      slideInRight: {
        "0%": {
          transform: "translateX(2rem)",
          opacity: "0",
        },
        "100%": {
          transform: "translateX(0)",
          opacity: "1",
        },
      },
      slideInTop: {
        "0%": {
          transform: "translateY(-2rem)",
          opacity: "0",
        },
        "100%": {
          transform: "translateY(0)",
          opacity: "1",
        },
      },
      slideInBottom: {
        "0%": {
          transform: "translateY(2rem)",
          opacity: "0",
        },
        "100%": {
          transform: "translateY(0)",
          opacity: "1",
        },
      },
      fadeIn: {
        "0%": {
          opacity: "0",
        },
        "100%": {
          opacity: "1",
        },
      },
      fadeOut: {
        "0%": {
          opacity: "1",
        },
        "100%": {
          opacity: "0",
        },
      },
      scaleIn: {
        "0%": {
          opacity: "0",
          transform: "scale(0)",
        },
        "100%": {
          opacity: "1",
          transform: "scale(1)",
        },
      },
      scaleOut: {
        "0%": {
          opacity: "1",
          transform: "scale(1)",
        },
        "100%": {
          opacity: "0",
          transform: "scale(0)",
        },
      },
      bounceRight: {
        "0%, 100%": {
          transform: "translateX(-25%)",
          animationTimingFunction: "cubic-bezier(0.8,0,1,1)",
        },
        "50%": {
          transform: "none",
          animationTimingFunction: "cubic-bezier(0,0,0.2,1)",
        },
      },
      bounceLeft: {
        "0%, 100%": {
          transform: "translateX(25%)",
          animationTimingFunction: "cubic-bezier(0.8,0,1,1)",
        },
        "50%": {
          transform: "none",
          animationTimingFunction: "cubic-bezier(0,0,0.2,1)",
        },
      },
      bounceBottom: {
        "0%, 100%": {
          transform: "translateY(25%)",
          animationTimingFunction: "cubic-bezier(0.8,0,1,1)",
        },
        "50%": {
          transform: "none",
          animationTimingFunction: "cubic-bezier(0,0,0.2,1)",
        },
      },
      heartbeat: {
        from: {
          transform: "scale(1)",
          transformOrigin: "center center",
          animationTimingFunction: "ease-out",
        },
        "10%": {
          transform: "scale(0.91)",
          animationTimingFunction: "ease-in",
        },
        "17%": {
          transform: "scale(0.98)",
          animationTimingFunction: "ease-out",
        },
        "33%": {
          transform: "scale(0.87)",
          animationTimingFunction: "ease-in",
        },
        "45%": {
          transform: "scale(1)",
          animationTimingFunction: "ease-out",
        },
      },
      dropIn: {
        "0%": {
          transform: "translateY(-500px)",
          animationTimingFunction: "ease-in",
          opacity: "0",
        },
        "38%": {
          transform: "translateY(0)",
          animationTimingFunction: "ease-out",
          opacity: "1",
        },
        "55%": {
          transform: "translateY(-65px)",
          animationTimingFunction: "ease-in",
        },
        "72%": {
          transform: "translateY(0)",
          animationTimingFunction: "ease-out",
        },
        "81%": {
          transform: "translateY(-28px)",
          animationTimingFunction: "ease-in",
        },
        "90%": {
          transform: "translateY(0)",
          animationTimingFunction: "ease-out",
        },
        "95%": {
          transform: "translateY(-8px)",
          animationTimingFunction: "ease-in",
        },
        "100%": {
          transform: "translateY(0)",
          animationTimingFunction: "ease-out",
        },
      },
      floating: {
        "0%,80%,100%": {
          transform: "translateY(0)",
        },
        "40%,50%": {
          transform: "translateY(-7%)",
        },
      },
      rotate: {
        "0%": {
          transform: "rotate(0)",
        },
        "100%": {
          transform: "rotate(360deg)",
        },
      },
      shake: {
        "0%,100%": {
          transform: "translateX(0)",
        },
        "10%,30%,50%,70%": {
          transform: "translateX(-10px)",
        },
        "20%,40%,60%": {
          transform: "translateX(10px)",
        },
        "80%": {
          transform: "translateX(8px)",
        },
        "90%": {
          transform: "translateX(-8px)",
        },
      },
      scaleInXLeft: {
        "0%": {
          transform: "scaleX(0)",
          transformOrigin: "0% 0%",
        },
        "100%": {
          transform: "scaleX(1)",
          transformOrigin: "0% 0%",
        }
      },
      scaleOutXLeft: {
        "0%": {
          transform: "scaleX(1)",
          transformOrigin: "0% 0%",
        },
        "100%": {
          transform: "scaleX(0)",
          transformOrigin: "0% 0%",
        }
      },
      scaleInXRight: {
        "0%": {
          transform: "scaleX(0)",
          transformOrigin: "100% 100%",
        },
        "100%": {
          transform: "scaleX(1)",
          transformOrigin: "100% 100%",
        }
      },
      scaleOutXRight: {
        "0%": {
          transform: "scaleX(1)",
          transformOrigin: "100% 100%",
        },
        "100%": {
          transform: "scaleX(0)",
          transformOrigin: "100% 100%",
        }
      },
      fireIn: {
        "0%": {
          opacity: "0",
          transform: "scale(2)",
          filter: "brightness(1000%)",
        },
        "100%": {
          opacity: "1",
          transform: "scale(1)",
          filter: "brightness(100%)",
        },
      },
      fireOut: {
        "0%": {
          opacity: "1",
          transform: "scale(1)",
          filter: "brightness(100%)",
        },
        "100%": {
          opacity: "0",
          transform: "scale(2)",
          filter: "brightness(1000%)",
        },
      },
    },
  },
  plugins: []
}
